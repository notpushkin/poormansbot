# Poor Man's Budget

Try it out on Telegram: [@poormansbot](https://t.me/poormansbot)

To run:

- create a Telegram bot (using [@BotFather](https://t.me/BotFather))
- copy `.env.example` to `.env` and edit
- `pip install -r requirements.txt`
- `honcho run flask run`
