from datetime import date, timedelta

from .models import db


def _int_from_str(s):
    buf = ""
    for c in s:
        if c in "0123456789":
            buf += c
    if buf == "":
        return None
    return int(buf)


def start(user, msg):
    yield "Привет!"
    yield "Я помогаю вести ежемесячный бюджет и откладывать."
    user.send_to("onboarding_1")
    yield "Для начала давай так: когда у тебя зарплата / стипуха / whatever?"


def onboarding_1(user, msg):
    user.payday = _int_from_str(msg["text"])
    if user.payday is None:
        yield "..."
    elif user.payday < 1 or user.payday > 28:
        yield "На всякий случай лучше указать день от 1 до 28."
    else:
        user.send_to("onboarding_2")
        yield "ОК! А сколько у тебя есть сейчас денег?"
        yield "Не бойся, я никому не скажу."


def onboarding_2(user, msg):
    amount = _int_from_str(msg["text"])
    if amount is None:
        yield "..."
    else:
        user.add_refill(amount)
        user.send_to("onboarding_3")
        yield "Сколько хочешь откладывать? В процентах."


def onboarding_3(user, msg):
    user.saving_percentage = _int_from_str(msg["text"])
    if user.saving_percentage is None:
        yield "..."
    else:
        user.send_to("main")
        yield "Отлично."
        yield "Пиши мне о своих покупках, например: «150 на шаверму» или " \
              "«пропил 1500», а я скажу, сколько ты ещё можешь потратить сегодня."
        yield "А сегодня ты можешь потратить..."
        yield "%d рублей!" % user.safe_to_spend


def main(user, msg):
    if msg["text"] == "/balance":
        sts = user.safe_to_spend
        if user.safe_to_spend >= 0:
            yield "Сегодня можешь потратить ещё %d рублей!" % sts
        else:
            yield "Сегодня ты ушёл в минус и больше тратить не можешь :("
            yield "У тебя −%d рублей." % (-sts)

    elif msg["text"] == "/debug":
        yield "`spent_today: %d`" % user.spent_today
        yield "`balance_today: %d`" % user.balance_today
        yield "`payday: %d`" % user.payday
        yield "`days_to_payday: %d`" % user.days_to_payday
        yield "`safe_to_spend: %d`" % user.safe_to_spend

    elif msg["text"] == "/nextday":
        for spend in user.spends:
            spend.date -= timedelta(days=1)
            db.session.add(spend)
        user.payday -= 1
        db.session.add(user)
        db.session.commit()
        yield "Time leap complete."
        yield "El. Psy. Congroo."

    elif msg["text"] == "/undo":
        spend = user.spends[-1]
        if spend.date == date.today():
            db.session.delete(spend)
            db.session.commit()
            yield "Отменил покупку за %d." % spend.amount
        else:
            yield "Сегодня ты ещё ничего не покупал :)"

    elif msg["text"] == "/reset":
        user.send_to("confirm_reset")
        yield "Точно хочешь сбросить? Напиши `ReSeT!` для продолжения."

    elif msg["text"] == "/help":
        yield "Случилось что-то непонятное?"
        yield "Напиши @iamale, он поможет!"
        # yield "(нет)"

    elif msg["text"].startswith("/refill"):
        num = _int_from_str(msg["text"])
        if num:
            user.add_refill(num, commit=True)
            yield "Грац! Записал: +%d" % num
        else:
            yield "Не понял тебя :( Попробуй написать числом, например, " \
                  "`/refill 1500`"

    else:
        num = _int_from_str(msg["text"])
        if num:
            user.add_spend(num, commit=True)
            yield "Записал! Потрачено: %d. (Отменить — /undo)" % num
            sts = user.safe_to_spend
            if sts > 0:
                yield "Можешь потратить ещё %d!" % sts
            else:
                yield "Сегодня ты ушёл в минус и больше тратить не можешь :("
                yield "У тебя −%d рублей." % (-sts)
        else:
            pass


def confirm_reset(user, msg):
    if msg["text"] == "ReSeT!":
        db.session.delete(user)
        db.session.commit()
        yield "Окей. Напиши /start, чтобы начать заново."
    else:
        user.send_to("main")
        yield "Удаление отменено."


def error(user, msg):
    yield "Ошибка! Ошибка! :("
