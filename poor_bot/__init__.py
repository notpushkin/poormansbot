from traceback import format_exc

from flask import Flask, request
import click

import requests

from . import routes, config
from .models import db, User


app = Flask(__name__)
app.config.from_object(config)

db.init_app(app)


def handle_message(msg):
    if "text" not in msg:
        return ["Прости, пока я понимаю только текстовые сообщения :("]

    user_id = msg.get("from", {}).get("id")
    user = User.get_or_create(user_id)
    return getattr(routes, user.route, routes.error)(user, msg)


@app.route("/handler", methods=["POST"])
def handle():
    api_root = "https://api.telegram.org/bot" + app.config["BOT_TOKEN"]
    data = request.get_json()
    if "message" not in data:
        return "no message"

    try:
        resp = list(handle_message(data["message"]))
    except:
        resp = ["```\n%s\n```" % format_exc()]

    for msg in resp:
        r = requests.post(api_root + "/sendMessage", json={
            "chat_id": data["message"]["chat"]["id"],
            "text": msg,
            "parse_mode": "markdown",
        })
        print(r.text)
        # r.raise_for_status()

    return ""


@app.cli.command()
def create_all():
    db.create_all()
    return "O. K."


@app.cli.command()
@click.argument("message")
def broadcast(message):
    api_root = "https://api.telegram.org/bot" + app.config["BOT_TOKEN"]

    for user in User.query.all():
        r = requests.post(api_root + "/sendMessage", json={
            "chat_id": user.id,
            # For user chats, it seems that user_id == chat_id for now.
            # Anyway we might support group chats at some point,
            # so we'll need to store chat_id instead of user_id.
            "text": message,
            "parse_mode": "markdown",
        })
        print(r.text)
        print(r.status_code)
    return "O. K."
