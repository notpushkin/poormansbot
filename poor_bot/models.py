from flask_sqlalchemy import SQLAlchemy
from datetime import date, timedelta


db = SQLAlchemy()

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    route = db.Column(db.String(80))
    payday = db.Column(db.Integer)
    saving_percentage = db.Column(db.Integer)

    def send_to(self, route, commit=True):
        self.route = route
        db.session.add(self)
        if commit:
            db.session.commit()

    @property
    def spent_today(self):
        """
        Sum of Spends made today.
        """
        today = date.today()
        q = db.session.query(db.func.sum(Spend.amount).label("total"))
        q = q.filter(Spend.user_id == self.id, Spend.date == today)
        return q.first()[0] or 0

    @property
    def balance_today(self):
        """
        Total amount of money user has in the beginning of the day.
        """
        today = date.today()

        q = db.session.query(db.func.sum(Refill.amount).label("total"))
        q = q.filter(Refill.user_id == self.id)
        had = q.first().total or 0

        # saving
        had *= 1 - (self.saving_percentage / 100)

        q = db.session.query(db.func.sum(Spend.amount).label("total"))
        # NB: we're not counting today's expenses here:
        q = q.filter(Spend.user_id == self.id, Spend.date < today)
        spent = q.first()[0] or 0

        return had - spent

    @property
    def days_to_payday(self):
        today = date.today()
        if today.day < self.payday:
            payday = today.replace(day=self.payday)
        elif today.month != 12:
            payday = today.replace(day=self.payday, month=today.month + 1)
        else:
            payday = today.replace(day=self.payday, year=today.year + 1)

        return (payday - today).days

    @property
    def safe_to_spend(self):
        """
        Sum that can be spent right now.
        """
        return self.balance_today / self.days_to_payday - self.spent_today

    def add_refill(self, amount, commit=False):
        refill = Refill(user_id=self.id, amount=amount)
        db.session.add(refill)
        if commit:
            db.session.commit()

    def add_spend(self, amount, commit=False):
        spend = Spend(user_id=self.id, amount=amount)
        db.session.add(spend)
        if commit:
            db.session.commit()

    @classmethod
    def get_or_create(cls, id):
        """
        If there isn't a user with that ID, create one and route them
        to "start".
        """
        user = cls.query.get(id)
        if user is None:
            user = cls(id=id, route="start")

        return user


class Refill(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer)  # > 0
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("User")
    user = db.relationship("User",
                           backref=db.backref("refills", lazy="dynamic",
                                              cascade="all,delete"))
    date = db.Column(db.Date, default=date.today)

    def __repr__(self):
        return "<Refill(%d at %r)>" % (self.amount, self.date)


class Spend(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer)  # > 0
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("User",
                           backref=db.backref("spends", lazy="dynamic",
                                              cascade="all,delete"))
    date = db.Column(db.Date, default=date.today)

    def __repr__(self):
        return "<Spend(%d at %r)>" % (self.amount, self.date)
