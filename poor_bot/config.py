import os

SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")
BOT_TOKEN = os.getenv("BOT_TOKEN")
